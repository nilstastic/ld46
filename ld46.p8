pico-8 cartridge // http://www.pico-8.com
version 18
__lua__


    house1 = {col=2, cables = {1,1,1,1,1,1,1}, makemoney=0, broken=false}
    house2 = {col=4, cables = {1,1,1,1,1,1,1}, makemoney=0, broken=false}
    house3 = {col=6, cables = {1,1,1,1,1,1,1}, makemoney=0, broken=false}
    house4 = {col=8, cables = {1,1,1,1,1,1,1}, makemoney=0, broken=false}
    houses = {house1,house2,house3,house4}
    
    mouse1 = {col=1, row=3, goal=house1, wait=0}
    mouse2 = {col=1, row=5, goal=house1, wait=0}
    mouse3 = {col=1, row=2, goal=house1, wait=0}
    mouse4 = {col=1, row=1, goal=house3, wait=0}
    --mouse5 = {col=1, row=1, goal=house4, wait=0}
    mice = {mouse1,mouse2,mouse3}--,mouse4}
    
    months = {"jan", "feb", "mars", "apr", "may", "june", "july", "aug", "sep", "oct", "nov", "dec"}
    currentmonth = 1
    gamestate = 0
    
    block=12

    alma = {col=5,row=8, repair=0, sign=0}
    f=0

    money = 0
    moneysign=0
function _init()
cls()


end

function drawcube(col,row,c)
    rect(col*block,row*block,col*block+block-1,row*block+block-1,c)
end


function resetgame()
    money=50
    currentmonth=1
    for house in all(houses) do
    house.cables = {1,1,1,1,1,1,1}
    house.broken = false
    house.makemoney = 0
    alma.row=8
    alma.col=5
    alma.repair=0
    alma.sign=0
    moneysign=0
    end

    for mouse in all(mice) do
        setmousepos(mouse)
    end

end

function _update()
    cls()
    if gamestate == 0 then
    
        print("KEEP [it] ALIVE",35,5,9)
        print("be the hero and fix",25,20,11)
        print("the broken cables",30,30,11)
  print("use arrows to move",27,40,8)
        print("working from home a whole year",5,50,12)
        print("vaccine ain't coming until", 12,60,12)
        print("next year",45,70,12)
        print("press (z) to start", 30,80,5)
      
        
        drawsprite(3,8,33)
        drawsprite(5,8,36)
        spr(33,6.5*block,8.1*block,2,2,true,false)
        if btnp(4) then 
            gamestate = 1
            resetgame()
        end
        return    
    end

if gamestate == 2 then
        print("congrats! you've made it!",13,10,9)
        print("it's dec and you've made $",5,20,9)
        print(money,113,20,20,14)
        print("you've earnd a holiday,",15,30,11)
        print("get the shot and have fun", 12,40,11)
    
        print("press (z) to restart", 20,60,5)
        if btnp(4) then 
            gamestate = 1
            resetgame()
        end
        return    
    end

    if gamestate == 3 then
        print("you have lost",30,10,12)
        print("no money, no internet, no work",0,20,11)
        print("it is "..months[currentmonth],15,30,11)
        --print(months[currentmonth],45,30,11)
       
        print("and you broke",60,30,11)
        print("try again?",40,50,12)
    
        print("press (z) to restart", 20,60,5)
        if btnp(4) then 
            gamestate = 1
            resetgame()
        end
        return    
    end

    f=f+1
    print("keep it alive",2,2,9)
    print(money,7,10,11)
     print("$",2,10,9)
   
    if(f%10==0)then
    alma.sign=alma.sign-1
    moneysign=moneysign-1
    end



 -- rita ut månader

print(months[currentmonth], 80,2,12)
  if (moneysign > 0) then
    print("-$50", 100,2,8)
    end

--rita ut husdollar
if house1.broken==false then
    print("$",house1.col*block+block,16,10)
end
if house2.broken==false then
    print("$",house2.col*block+block,16,10)
end
if house3.broken==false then
    print("$",house3.col*block+block,16,10)
end
if house4.broken==false then
    print("$",house4.col*block+block,16,10)
end



--earn money
    if(f%30==0)then
        for i=1, 4 do
            updatemoneyfromhouse(houses[i])
        end
    end


--mus

    for mouse in all(mice) do
        if(f%33==0) then
            
            if(mouse.wait > 0)then
                -- musen väntar
                mouse.wait=mouse.wait-1
            else
                --musen går åt höger
                mouse.col +=1
        
            end

            --musen äter kabel och adderar till wait
            if(mouse.col==mouse.goal.col and mouse.goal.cables[mouse.row-1]==1) then     
                mouse.wait=2
                mouse.goal.cables[mouse.row-1]=0
                mouse.goal.broken=true
            end

            if(mouse.col>9) then
            mouse.col=1 
            mouse.row=flr(rnd(7)) +2  
            mouse.goal=houses[flr(rnd(4))+1] 
            end   
        
    end
end
    --alma styrs
    if(btnp(0)) and alma.col>0 then
        alma.col=alma.col-1
        alma.repair=0
    end
    if(btnp(1)) and alma.col<9 then
        alma.col=alma.col+1
        alma.repair=0
    end
    if(btnp(2)) and alma.row >0 then
        alma.row=alma.row-1
        alma.repair=0
    end
    if(btnp(3)) and alma.row <9 then
        alma.row=alma.row+1
        alma.repair=0
    end

    --alma lagar
    if(f%10==0) then
        alma.repair=alma.repair+1
    end
    drawhouse(house1)
    drawhouse(house2)
    drawhouse(house3)
    drawhouse(house4)
    drawmouse(mouse)
    drawalma(alma)
    for i=1,4 do 
        if(houses[i].col==alma.col and houses[i].cables[alma.row-1]==0) then
            line(alma.col*block,alma.row*block,alma.col*block+alma.repair,alma.row*block,11)
            if(alma.repair>=10) then
                houses[i].cables[alma.row-1]=1
                money=money-25
                alma.sign=3
                
                houses[i].broken=ishousebroken(houses[i])
                
                
            end
        end
    end
    drawenviroment()
    checktimestatus()

    if money<1 then
      gamestate=3
    end
end

function setmousepos(mouse)
   -- for mouse in all(mice)do
            mouse.col=1
            mouse.row=flr(rnd(7)) +2  
            mouse.goal= houses[flr(rnd(4))+1] 
            mouse.wait = flr(rnd(7)) +2  
    --end
end

function checktimestatus()
    if currentmonth<12 then
        if(f%(10*30)==0) then
        currentmonth = currentmonth+1
        --månadskostnad
        money=money-50
        moneysign=3
        end
    else if currentmonth==12 and money>0 then
        gamestate=2
        end
        end
end

function ishousebroken(house)
    for i=1, 7 do  
        if house.cables[i] ==0 then
            return true
        end
    end
    return false
end

function updatemoneyfromhouse(house)
    
        if(house.broken==true)then
        return   
       
    end
        money=money+5
end

function drawmouse(mouse)
    for mouse in all(mice) do
       -- drawcube(mouse.col,mouse.row,4)
         drawsprite(mouse.col,mouse.row,33,8,8)
       
    end
end

function drawsprite(col,row,s,w,h)
    x = col*block
    y = row*block
    spr(s,x,y,2,2)

--    spr(s,col*block,row*block,col*block+block-1,row*block+block-1)
end

function drawenviroment()
    --buske
    --rectfill(0,0,100,100,6)
    for i = 1, 7 do
    --drawcube(0,i,14)
        drawsprite(1,i+1,14)
    end
    --kontoret
    for i= 1,8 do
    drawsprite(i,9,9)
    end
end

function drawalma(alma)
    --drawcube(alma.col,alma.row,6)
    drawsprite(alma.col,alma.row,38)
    if (alma.sign > 0) then
    print("-$25",alma.col*block+5,alma.row*block-5,8)
    end
end

function drawhouse(building)
if building.broken then
   -- drawcube(building.col,1,4)
    drawsprite(building.col,1,4)
    else
     --drawcube(building.col,1,3)
      drawsprite(building.col,1,36)
     end
    for i = 1, 7 do
        if building.cables[i]==1 then
             --drawcube(building.col,i+1,5)
             drawsprite(building.col,i+1,12,1,1)
         else 
             drawsprite(building.col,i+1,44,1,1)
        end
    end
end
__gfx__
0000000000000000000000000000000000000bb000000000055555550000000000000000ddddddddddddddd00000000000005600000000000000000000000000
0000000000005550000000000000000000eeeeee00000000050050050000000000000000ddadadaddddadad00000000000005600000000000000333300000000
000000000005566600000000000000000eeeeeeee0000000050050050000000000000000ddadadaddddadad00000000000005600000000003330bb3333000000
00000000005566160000000000000000eeeeeeeeee000000055555550000000000000000ddadadaddddadad0000000000000056000000000333bbbbb33300000
00000000566666660000000000000000eeeeeeeeee000000050050050000000000000000ddddddddddddddd0000000000000056000000000333bb2bb33300000
000000000000000000000000000000000555555550000000050050050000000000000000ddadddadaddddad0000000000000056000000000333bbbb332333000
000000000000000000000000000000000550000550000000055555550000000000000000ddadddadaddddad000000000000056000000000003bbbbb333333000
000000000000000000000000000000000550000550000000050050050000000000000000ddadddadaddddad0000000000000560000000000333bbbb3bbbb3000
000000000000000000000000000000000550000550000000050050050000000000000000ddddddddddddddd0000000000000560000000000333bb2b3b2bb3000
000000000000000000000000000000000555555550000000055555550000000000000000ddadadadddadadd0000000000000056000000000333bbbbbbbb33000
000000000000000000000000000000000555555550000000055555550000000000000000ddadadadddadadd0000000000000056000000000003bbbbbbbb33000
000000000000000000000000000000000000000000000000055555550000000000000000ddadadadddadadd000000000000005600000000000333bbbb3330000
000000000000000000000000000000000000000000000000000000000000000000000000ddddddddddddddd0000000000000560000000000003333bbb3330000
000000000000000000000000000000000000000000000000000000000000000000000000ddddddddddddddd00000000000000000000000000000033333000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000330000000
00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000
0000000000000000000000000000000000000bb00000000000000000000000000000000000000000000000000000000000005600000000000000000000000000
0000000000000000000000000000000000eeeeee0000000000000000000000000000000000000000000000000000000000005600000000000000000000000000
000000000000000000000000000000000eeeeeeee00000000000aaaa000000000000000000000000000000000000000000005600000000000000000000000000
00000000000000000000000000000000eeeeeeeeee000000000aa909900000000000000000000000000000000000000000000560000000000000000000000000
00000000000022222000000000000000eeeeeeeeee00000000aa9999900000000000000000000000000000000000000000070560070000000000000000000000
00000000000222232200000000000000055555555000000000aa9999806600000000000000000000000000000000000000000000000000000000000000000000
000000000002223b2200000000000000055999a55000000000aa0990006000000000000000000000000000000000000007000000700000000000000000000000
000000002222222222200000000000000559aaa5500000000aa00bb0006600000000000000000000000000000000000000000000000000000000000000000000
000000000006060006000000000000000559aaa55000000000000bbbb56000000000000000000000000000000000000000705600070000000000000000000000
0000000000000000000000000000000005555555500000000000bbb0006000000000000000000000000000000000000000000560000000000000000000000000
0000000000000000000000000000000005555555500000000000bbb0000000000000000000000000000000000000000000000560000000000000000000000000
0000000000000000000000000000000000000000000000000000bbbb000000000000000000000000000000000000000000000560000000000000000000000000
00000000000000000000000000000000000000000000000000000b03000000000000000000000000000000000000000000005600000000000000000000000000
00000000000000000000000000000000000000000000000000000303000000000000000000000000000000000000000000000000000000000000000000000000
